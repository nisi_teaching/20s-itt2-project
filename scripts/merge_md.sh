#!/usr/bin/env bash

if [ "x" == "x$2" ]; then
	echo "merge all <basename>_ww??.md files with prepend and append"
	echo "usage: $0 <basename> <reportname>"
	exit 1
fi

FNAME=$2
BNAME=$1
FHEAD="${BNAME}_prepend.md"
FFOOT="${BNAME}_append.md"

rm -f $FNAME.md


echo "Combining md files" > /dev/stderr
echo "- adding header $FHEAD" > /dev/stderr
cat $FHEAD >> $FNAME.md

for F in ${BNAME}_ww??.md; do
	echo "- adding $F" > /dev/stderr
    echo "" >> $FNAME.md
	sed '1 { /^---/ { :a N; /\n---/! ba; d} }' $F >> $FNAME.md
    echo "" >> $FNAME.md

	echo "\\pagebreak " >> $FNAME.md
done;

echo "- adding footer $FFOOT" > /dev/stderr
cat $FFOOT >> $FNAME.md
