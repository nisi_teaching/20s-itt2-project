---
Week: 07
Content:  proof of concept 2/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 07

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Electronic modules completed and working
* Able to read voltages from Boron's ADC

### Learning goals

* Electronic modules
  * Level 1: Able to build and document, electronic modules from instructions
  * Level 2: Able to simulate modules with OrCad/PSpice
  * Level 3: Able to perform physical tests according to simulation

* Analog to digital (ADC) conversion
  * Level 1: Able to setup a simple program tht samples a voltage
  * Level 2: Able to calculate voltage/current values from ADC values
  * Level 3: Able to use the datasheet when working with Boron's ADC

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)
* Exercise documentation included in Gitlab project

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Teacher meetings - see agenda|
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |


### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)
