---
Week: 23
Content:  part 2 - Make it useful 2/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 23

For this last phase of part 2, you are to make your system useful for your users.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

None at the moment

### Learning goals

None at the moment

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

* Exercise documentation included in Gitlab project

## Schedule


### Monday

This is Whit Monday, a Holiday.


### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Meeting room [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456** is open, teachers available as much as possible |
|       | Guide on how to use zoom are in the comments below |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | **During this meeting please keep your camera *enabled* and microphone *disabled*** |
|       | To join the video meeting, click this link: [https://ucldk.zoom.us/join](https://ucldk.zoom.us/join) Meeting ID: **615 3388 1974** Password: **123456** |
|       | Video recording from the meeting: [google docs](https://drive.google.com/drive/folders/1CITE7Ph0YWUUNZYBqW2cpD8y7IKZnGXy?usp=sharing) |
| 9:30  | Group morning meetings - use your virtual group room |
| 10:00 | Hands-on time (you work on issues) |
| 15:30 | End of day |


## Hands-on time

Please consider that we have project presentations next week and you need to finish the technical issues you have in the project.

## Comments
* This week is special. Because of whit monday we do not have teacher meetings. 


* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)
