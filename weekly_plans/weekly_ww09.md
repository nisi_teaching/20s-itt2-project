---
Week: 09
Content:  consolidation 1/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 09

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* node-red dashboard installed and configured on RPi

### Learning goals

* Dashboard
  * Level 1: Able to setup node-red dashboard
  * Level 2: Able to show two way serial communication in the dashboard
  * Level 3: Able to show button status in dashboard

* Automated testing
  * Level 1: Can explain .gitlab-ci.yml file
  * Level 2: Reuse existing CI pipeline in new project
  * Level 3: Implement a CI pipeline in gitlab

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)
* Exercise documentation included in Gitlab project

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Teacher meetings - see agenda|
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |


### Tuesday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | Group morning meetings |
| 9:00  | Hands-on time (you work on exercises) |
| 15:30 | End of day |

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)
