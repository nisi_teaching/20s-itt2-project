---
Week: 12
Content:  make it useful for the user 1/2
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 12

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Virtual room for group meetings established and documented in project plan
* Particle cloud functions implemented

### Learning goals

* Particle cloud function
  * Level 1: The student is able to implement a simple cloud function  
  * Level 2: The student is able to implement an advanced cloud function (ADC)
  * Level 3: The student is able to implement an advanced cloud function (ADC) in the node-red-dashboard

## Deliverables

* Daily group morning meeting - see week 05 for agenda
* Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

* Exercise documentation included in Gitlab project

## Schedule

### Monday

| Time  | Activity |
| :---- | :--- |
| 8:15  | Setup a virtual room at meet.google.com to perform group video meetings |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | **During this meeting please keep your camera and microphone disabled unless asked to turn it on** |
|       | Virtual room at meet.google.com is called **itt_project_common** |
|       | To join the video meeting, click this link: [https://meet.google.com/hpr-axco-goi](https://meet.google.com/hpr-axco-goi) |
| 9:30  | Group morning meetings - use your virtual group room |
| 10:00 | Teacher meetings - see agenda and timetable |
| 10:00 | Hands-on time (you work on exercises) |
| 15:30 | End of day |

### Teacher meeting timetable  

| Time  | itt_project_room1 | itt_project_room2 |
| :---- | :----- | :----- |
| 10:00 | group1 | group6 |
| 10:15 | group2 | group7 |
| 10:30 | group3 | group8 |
| 10:45 | group4 | group9 |
| 11:00 | group5 | group10 |

* Virtual rooms for teacher meetings

    We have created two seperate rooms on meet.google.com for teacher meetings.

    We will message your group on Riot about when you should connect to the room, so please pay attention to Riot and the timetable the entire day.

    For teacher meetings we would like you to have your camera active all the time and your microphone active only when you would like to speak.     

    Connection details for the two rooms are:  

    **itt_project_room1**  
      To join the video meeting, click this link: [https://meet.google.com/ibg-nita-oaz](https://meet.google.com/ibg-nita-oaz)  

    **itt_project_room2**  
      To join the video meeting, click this link: [https://meet.google.com/yey-khwo-hhp](https://meet.google.com/yey-khwo-hhp)  

### Tuesday

| Time  | Activity |
| :---- | :--- |
| 9:00  | Introduction to the day, general Q&A session, decide on extra session if applicable |
|       | **During this meeting please keep your camera and microphone disabled unless asked to turn it on** |
|       | Virtual room at meet.google.com is called **itt_project_common** |
|       | To join the video meeting, click this link: [https://meet.google.com/hpr-axco-goi](https://meet.google.com/hpr-axco-goi) |
| 9:30  | Group morning meetings  - use your virtual group room |
| 9:30  | Hands-on time (you work on exercises) |
| 15:30 | End of day |

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_exercises.pdf) for exercises details

## Comments
* Google hangouts meet guide [https://ucl-lte.screenstepslive.com/s/22472/m/94395/l/1215644-students-how-to-participate-in-google-hangouts-meet](https://ucl-lte.screenstepslive.com/s/22472/m/94395/l/1215644-students-how-to-participate-in-google-hangouts-meet)

* Companion site [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)

* Not meeting in person presents problems for group work. Ensure that your issues are updated and that you have multiple functioning communicatoin channels.
