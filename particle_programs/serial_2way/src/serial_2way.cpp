/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "application.h"
#line 1 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/serial_2way/src/serial_2way.ino"
/*
 * Project serial_2way
 * Description: serial between Boron, node-red and putty
 * Author: NISI
 * Date: 2020-02-12
 */

void setup();
void loop();
#line 8 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/serial_2way/src/serial_2way.ino"
const unsigned long UPDATE_PERIOD_MS_1 = 1000; //1mS period
unsigned long lastUpdate;
unsigned long counter; 

// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
  Serial1.begin(9600);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // Send data in intervals
  if (millis() - lastUpdate >= UPDATE_PERIOD_MS_1) {
		lastUpdate = millis();
    Serial1.printlnf("sending test data %d", ++counter);
  }

  // if serial data received on Boron rx pin,  print to serial (USB)
  while (Serial1.available() > 0) {
      String incoming = Serial1.readStringUntil('\n');
      Serial.printlnf("received from node-red: " + incoming);  
  }
}