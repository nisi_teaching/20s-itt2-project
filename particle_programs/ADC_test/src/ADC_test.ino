/*
 * Project ADC_test
 * Description: Read analog value from A0 input
 * Author: NISI
 * Date: 2020-02-06
 */

int analog_in = A0; //initialize analog input A0 to variable
int analogValue; //used to store analog readings

void setup() {
  Serial.begin(9600); //used for debugging over USB
}
 
void loop() {
  analogValue = analogRead(analog_in); //read the analog input
  float voltage = analogValue * (3.3/4096.0);
  Serial.printlnf("%f", voltage); //print the ADC value to USB
  delay(100); //blocking delay 100ms
}