/*
 * Project boron_publish
 * Description:
 * Author:
 * Date:
 */

// token: 1213101cd4738c5717a052c2e4aadc0977559624 expires Fri Jun 05 2020 20:32:11 GMT+0200
SYSTEM_THREAD(ENABLED);
int counter = 0;
double current = 0;

void setup() {
  Particle.variable("curr", current);
}

void loop() {
  current = counter;
  counter++;
  delay(200);
}