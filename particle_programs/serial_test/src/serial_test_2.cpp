/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "application.h"
#line 1 "c:/Users/localadmin/Documents/personal_gitlab/particle_test/serial_test_2/src/serial_test_2.ino"
/*
 * Project serial_test_2
 * Description:
 * Author:
 * Date:
 */

void setup();
void loop();
#line 8 "c:/Users/localadmin/Documents/personal_gitlab/particle_test/serial_test_2/src/serial_test_2.ino"
int counter = 0;

// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
  Serial1.begin(9600);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here.
  Serial1.printlnf("Hello from Boron! message no. %d", ++counter);
	delay(1000);
}