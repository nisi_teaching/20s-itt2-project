/*
 * Project Boron2_blink
 * Description:
 * Author:
 * Date:
 */

int led2 = D7;

// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
  pinMode(led2,OUTPUT);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here.
  digitalWrite(led2, HIGH);
  Particle.publish("led2_status", "ON", 60, PRIVATE);
  delay(1000);
  digitalWrite(led2, LOW);
  Particle.publish("led2_status", "OFF", 60, PRIVATE);
  delay(1000);
}