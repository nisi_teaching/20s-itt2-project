/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "application.h"
#line 1 "c:/Users/localadmin/Documents/personal_gitlab/particle_test/Boron2_blink/src/Boron2_blink.ino"
/*
 * Project Boron2_blink
 * Description:
 * Author:
 * Date:
 */

void setup();
void loop();
#line 8 "c:/Users/localadmin/Documents/personal_gitlab/particle_test/Boron2_blink/src/Boron2_blink.ino"
int led2 = D7;

// setup() runs once, when the device is first turned on.
void setup() {
  // Put initialization like pinMode and begin functions here.
  pinMode(led2,OUTPUT);
}

// loop() runs over and over again, as quickly as it can execute.
void loop() {
  // The core of your code will likely live here.
  digitalWrite(led2, HIGH);
  Particle.publish("led2_status", "ON", 60, PRIVATE);
  delay(1000);
  digitalWrite(led2, LOW);
  Particle.publish("led2_status", "OFF", 60, PRIVATE);
  delay(1000);
}