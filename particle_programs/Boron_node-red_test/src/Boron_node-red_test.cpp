/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "application.h"
#line 1 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/Boron_node-red_test/src/Boron_node-red_test.ino"
/*
 * Project: boron_node-red_test
 * Description: Interaction between Boron and node-red through serial communication
 * Author: NISI
 * Date: 2020-02-12
 */

/* 
 * Pseudo-code:
 * 
 * every 1 sec:
 *    send ADC value
 * 
 * if btn_press:
 *    send 'VAL, ID:1, VA: btn1'
 * 
 * while serial available:
 *    if CMD == LED: 1
 *      turn on led
 *    if CMD == LED: 0
 *      turn off led
 *
 * Protocol:
 * 0 = ERR
 *    
 * 1 = CMD
 *    1 = LED
 *      0 = OFF
 *      1 = ON
 * 
 * 
 * 
 */

void setup();
void loop();
#line 35 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/Boron_node-red_test/src/Boron_node-red_test.ino"
const unsigned long UPDATE_PERIOD_MS = 1000, UPDATE_PERIOD_MS_2 = 250; //periods
unsigned long lastUpdate, lastUpdate2; //timer checks
unsigned long counter; 
int led_1 = D7;
int button = D2;
int analog_in = A0; //initialize analog input A0 to variable
int analogValue; //used to store analog readings
int btn_val = 1;
int btn_msg = 1;

// function prototypes


void setup() {
  // Put initialization like pinMode and begin functions here.
  Serial1.begin(9600);
  pinMode(led_1, OUTPUT);
  pinMode(button, INPUT_PULLUP);
}

void loop() {
  // Send data in intervals
  if (millis() - lastUpdate >= UPDATE_PERIOD_MS) {
		lastUpdate = millis();
    // Serial1.printlnf("sending test data %d", ++counter);
    analogValue = analogRead(analog_in); //read the analog input
    float voltage = analogValue * (22/4096.0);
    if (btn_val == 0){
      btn_msg = 0;
      btn_val = 1;
    } 
    Serial1.printlnf(String(voltage) + "," + String(btn_msg));
    btn_msg = 1;
  }

  if (millis() - lastUpdate2 >= UPDATE_PERIOD_MS_2) {
    lastUpdate2 = millis();
    btn_val = digitalRead(button);       
  }



  // if serial data received on Boron rx pin,  print to serial (USB)
  while (Serial1.available() > 0) {
      String incoming = Serial1.readStringUntil('\n');
      // Serial.printlnf("received from node-red: " + incoming);  
      if (incoming == "111"){
        digitalWrite(led_1, HIGH);
        //Serial.printlnf("LED ON");
      }
      else if (incoming == "110"){
        digitalWrite(led_1, LOW);
        //Serial.printlnf("LED OFF");
      }
  }
}