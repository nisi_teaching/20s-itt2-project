/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/boron_function/src/boron_function.ino"
void setup();
void loop();
int led1Control(String command);
#line 1 "c:/Users/localadmin/Documents/EAL-ITT/20s-itt2-project/particle_programs/boron_function/src/boron_function.ino"
SYSTEM_THREAD(ENABLED); // enable system threading
int led1 = D7; //associate gpio D7 with led1

void setup() {
  Particle.function("led1", led1Control); //setup cloud function
  pinMode(led1,OUTPUT); //initialize gpio D7

}

void loop() {

}

int led1Control(String command){
  if (command == "led1_on"){
    digitalWrite(led1, HIGH); //gpio D7 on 
    Particle.publish("led1_status", "ON", 60, PRIVATE); //publish led1 status to the cloud
    return 1; //if statement executed ok
  }
  else if (command == "led1_off")
  {
    digitalWrite(led1, LOW); //gpio D7 off
    Particle.publish("led1_status", "OFF", 60, PRIVATE); //publish led1 status to the cloud
    return 1; //if statement executed ok
  }  
  else
  {
    Particle.publish("led1_status", "ERROR", 60, PRIVATE); //publish led1 status to the cloud
    return -1; //something went wrong
  }
}
