---
title: '20S ITT2 Project'
subtitle: 'Lecture plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, 20S
* Name of lecturer and date of filling in form: NISI, MON 2020-12-03
* Title of the course, module or project and ECTS: Semester project, 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


| Teacher   |  Week     | Content                                       |
|:----------|:----------|:----------------------------------------------|
|  MON/NISI | 5         | Project part 1 - Project setup and minimal system
|           |           | Introducing the course and the project.       
|           |           | Forming teams
|           |           | Setup communication with data science teams
|           |           | Project plan 
|           |           | Pre Mortem
|           |           | 
|           |           | Overview of the minimal system:
|           |           | Digital-Analog-Particle-Cloud-RPi-Node Red
|           |           | 
|           |           | Gitlab setup
|           |           | Weekly structure
|           |           | Company presentation
|  MON/NISI | 6         | Project part 1 - Phase 1: Proof of concept
|           |           | Technical requirements
|           |           | Setup up documentation (Blockdiagram, schematic, flowchart etc.)
|           |           | Have a clear idea before doing any technical work!
|           |           | Explore the particle eco system:
|           |           |   
|           |           | Setup Boron device
|           |           | Setup workbench IDE
|           |           | Setup particle product and include team members
|           |           | 
|           |           | First Boron sketch: Blink an LED
|  MON/NISI | 7         | Continue work on minimal system.
|           |           | Build and test 5 - 20mA simulator module
|           |           | Build and test 5 - 20mA interface module
|           |           | Setup and test Boron sampling sketch
|  MON/NISI | 8         | Continue work on minimal system.
|           |           | Build and test button module
|           |           | Setup Boron button sketch
|           |           | Setup Node red on Raspberry Pi
|           |           | Establish serial communication between Boron and Node red 
|  MON/NISI | 9         | Project part 1 - Phase 2: Consolidation
|           |           | Plan Node red dashboard interface for minimal system
|           |           | Update documentation
|           |           | Implement Gitlab pages 
|  MON/NISI | 10        | Continue work on minimal system.
|           |           | Connect Button and 5 - 20mA modules to Node red
|  MON/NISI | 11        | Continue work on minimal system.
|  MON/NISI | 12        | Project part 1 - Phase 3: Make it useful for the user
|           |           | Research Hydac R&D requirements (what they need in the dashboard)
|  MON/NISI | 13        | Continue work on minimal system.
|           |           | Implement Hydac R&D requirements in dashboard
|  MON/NISI | 14        | Continue work on minimal system.
|           |           | Build enclosure for hardware simulation modules
|           |           | OLA23 first attempt
|  MON/NISI | 15        | No lectures
|  MON/NISI | 16        | Project part 2 - Phase 1: Proof of concept
|           |           | Monday 2020-04-13 is Easter Monday, no lectures this day
|           |           | Design your own system with respect to the system requirements from Hydac.
|           |           |
|           |           | Expand project plan to include part 2
|           |           | Generic ideas:
|           |           |
|           |           | Hardware status LED
|           |           | Monitoring of services
|           |           | Boron/Xenon mesh network
|           |           | Interface to other bus industry standard 
|           |           | Webservice connecting to REST API
|           |           | Integrate particle cloud to e.g. AWS
|           |           |
|  MON/NISI | 17        | TBD by students and teachers
|  MON/NISI | 18        | TBD by students and teachers 
|  MON/NISI | 19        | Project part 2 - Phase 2: Consolidation
|  MON/NISI | 20        | TBD by students and teachers
|  MON/NISI | 21        | TBD by students and teachers 
|  MON/NISI | 22        | Project part 2 - Phase 3: Make it useful for the user
|  MON/NISI | 23        | TBD by students and teachers
|           |           | Monday 2020-06-01 is Whit Monday, no lectures this day
|  MON/NISI | 24        | TBD by students and teachers
|           |           | Project presentations
|  MON/NISI | 25        | No lectures - prepare for exam
|  MON/NISI | 26        | Exams

# General info about the course, module or project

## The student’s learning outcome

Learning goals for the course are described in the curriculum section 2 about the national elements.

Besides consolidating the material from the other course, a large part of the project is directly related to the topic about project management (section 2.4 in the curriculum).
See the curriculum for more details

## Content

The course is formed around a project, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation.

## Method

The project will be divided into blocks that build upon each other. The idea is to create a system that works from sensor to presentation in the cloud while the students practice project management and learn operational skills.  
![project overview](../docs/photos/generic_project_overview.png "project overview")  

## Equipment

* Particle development boards (Boron and Xenon)
* Raspberry Pi
* Level shifter
* Laptop able to run Visual Studio Code

## Projects with external collaborators  (proportion of students who participated)

External collaborators:  

[Hydac A/S](https://www.hydac.com/de-en/start.html)  

[Computer science education](https://www.ucl.dk/uddannelser/videregaaende-uddannelser/datamatiker)  

All students will participate.

## Test form/assessment
The project includes 1 compulsory element.

See semester description on itslearning for details on compulsory elements.

## Other general information
None at this time.
