Groups
===

## `20S-itt2-project`


- **Location:** Room A
- **Date:** 2020-01-27
- **Group_1**  
	gitlab handle: @jens934x, school mail: jens365y@edu.ucl.dk  
	gitlab handle: @alex8684, school mail: alex8684@edu.ucl.dk  
	gitlab handle: @toastin, school mail: mads545b@edu.ucl.dk  
	gitlab handle: @runetoftlund, school mail: rune7705@edu.ucl.dk  

- **Group_2**  
	gitlab handle: @jonasOODK, school mail: jona415t@edu.ucl.dk  
	gitlab handle: @Sambakongen, school mail: thom37q6@edu.ucl.dk  
	gitlab handle: @mikk744e, school mail: mikk744e@edu.ucl.dk  
	gitlab handle: @JacobPloeger, school mail: jaco223r@edu.ucl.dk  

- **Group_3**  
	gitlab handle: @.Franco, school mail: fran5040@edu.ucl.dk  
	gitlab handle: @pietmat, school mail: mate1180@edu.ucl.dk  
	gitlab handle: @adam5481, school mail: adam5520@edu.ucl.dk  
	gitlab handle: @gytis.valatkevicius, school mail: gyti0045@edu.ucl.dk  

- **Group_4**  
	gitlab handle: @aabols, school mail: niki1422@edu.ucl.dk  
	gitlab handle: @Victoria570, school mail: vict570g@edu.ucl.dk  
	gitlab handle: @STuska, school mail: soph7411@edu.ucl.dk  
	gitlab handle: @yani0101, school mail: yani0101@edu.ucl.dk  

- **Group_5**  
	gitlab handle: @anto8460, school mail: anto8588@edu.ucl.dk  
	gitlab handle: @Dani35b6, school mail: dani35b6@edu.ucl.dk  
	gitlab handle: @mohosseinn, school mail: moha4424@edu.ucl.dk  
	gitlab handle: @mart10t7, school mail: mart56t6@edu.ucl.dk  

- **Group_6**  
	gitlab handle: @SebSebSeb, school mail: seba164i@edu.ucl.dk  
	gitlab handle: @Adham0, school mail: adha0051@edu.ucl.dk  
	gitlab handle: @Simongj, school mail: simo5891@edu.ucl.dk  
	gitlab handle: @ivox005, school mail: ivox0035@edu.ucl.dk  

- **Group_7**  
	gitlab handle: @djmarjo2000, school mail: mari30hs@edu.ucl.dk  
	gitlab handle: @domo011, school mail: domo0013@edu.ucl.dk  
	gitlab handle: @barn0083, school mail: barn0087@edu.ucl.dk  
	gitlab handle: @bali0060, school mail: balint0061@edu.ucl.dk  

- **Group_8**  
	gitlab handle: @veysula, school mail: veys0053@edu.ucl.dk  
	gitlab handle: @cristianguba, school mail: cris1288@edu.ucl.dk  
	gitlab handle: @mikk09d2, school mail: mikk09d2@edu.ucl.dk  
	gitlab handle: @fadi91, school mail: fadi0060@edu.ucl.dk  

- **Group_9**  
	gitlab handle: @meenamsp, school mail: meen0037@edu.ucl.dk  
	gitlab handle: @OwlinABox, school mail: nico835t@edu.ucl.dk  
	gitlab handle: @diog0043, school mail: diog0044@edu.ucl.dk  
	gitlab handle: @cami274t, school mail: cami274t@edu.ucl.dk  

- **Group_10**  
	gitlab handle: @jjffnn, school mail: jona8046@edu.ucl.dk  
	gitlab handle: @jaco4200, school mail: jaco4200@edu.ucl.dk  
	gitlab handle: @rasm702e, school mail: rasm702e@edu.ucl.dk  
	gitlab handle: @lass3728, school mail: lass3728@edu.ucl.dk  