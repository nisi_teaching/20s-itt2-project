---
Week: 22
tags:
- Make it useful
- Documentation
---


# Exercises for ww22

## Exercise 1 - Define "Make it useful" goals

Consolidation phase ended in week 21 and we are now in the "Make it useful for the user" phase.

In order for you to structure your work, you must have a clear idea about the goal. This you must define and document.

## Instructions

1. In the group, discuss which features and documentation your will present to your end user.

2. Make some tasks (gitlab issues), diagrams and supporting text to document the system-to-be.

At the end of this phase you will be asked to make a guided presentation for you potential users.

## Exercise 2 - Define "Make it useful" milestones

To practice making milestones you need to make a milestone for at least each week in the "Make it useful for the user" phase

It might be that it fits your project to make milestones grouped by subject like "Housing" or "guided presentation". It might be that it fits your project better to make milestone for each week, "Week 22", "Week 23" etc.
It is up to you to decide this in the group. 

## Instructions

1. In your group identify logic grouping of your "Make it useful for the user" tasks.

2. Create milestones on gitlab for each logical group of tasks