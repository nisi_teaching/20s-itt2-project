---
Week: 09
tags:
- node-red
- dashboard
- test
---

# Exercises for ww09

## Exercise 1 - node-red dashboard

### Information

This exercise is about setting up the node-red-dashboard as well as visualizations and control of the LED/button module and ADC reading from the current loop tester module. 
The purpose is to have a local dashboard used for troubleshooting while developing, the dashboard functionality will be expanded the coming weeks.

### Exercise instructions

Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_dashboard](https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_setup)  

## Exercise 2 - Automated static test

### Information

We want to start implementing automated testing of the code you write.

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/docs/particle/automated_tests/](https://eal-itt.gitlab.io/iot-with-particle/docs/particle/automated_tests/)

If you get any errors or warnings, fix them.

If you don't get any, try implementing one, like add an unused variable.
