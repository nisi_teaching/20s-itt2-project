---
Week: 21
tags:
- Consolidation
- Documentation
- Feedback
---

# Exercises for ww21

## Exercise 1 - Consolidation video and feedback

Consolidation phase ends this week (21) and we would like you to do a video to show and share what you accomplished during consolidation.

You need to produce a video ~3 minutes long, that includes a walkthrough of what you accomplished in the consolidation phase.  
The video also needs to include an evaluation of your consolidation goals you defined in week 20.  
The video should be uploaded to youtube or similar and shared with teachers and the other project groups.
We are not experts in video editing software, however a quick google search revealed [https://www.openshot.org/](https://www.openshot.org/) that works for mac, linux and windows.
Something like [https://obsproject.com/](https://obsproject.com/) might also work for you.

## Instructions

1. Setup your system
2. Make a video that explains the moving parts and a small demo of the system (the video needs to have an introduction slide at the beginning and a contact slide at the end).
3. Show a list of your consolidation goals from week 20, show how many you completed and explain why you didn't manage to reach all of them.
4. Share your video with teachers and other groups on [google docs](https://docs.google.com/document/d/1uAy6h8GBK0l79DGOa8TuzoiPMe1Uf4355xcTpvqDsHQ/edit?usp=sharing)  
The deadline for submission of the video is at 2020-05-19 14:00.
5. Before 2020-05-19 16:00 all group members must watch the other groups videos and give feedback on [google docs](https://docs.google.com/document/d/1uAy6h8GBK0l79DGOa8TuzoiPMe1Uf4355xcTpvqDsHQ/edit?usp=sharing).  
While you watch these videos please note what kind of inspiration it gives you regarding your own project and your thoughts about what could be optimized in the project presented in the video.  
Please write your thougts for each video in the comment section directly below the individual video links, remember to include your name at the end of your comment.


