---
Week: 11
tags:
- node-red
- Cloud
- Documentation
---

# Exercises for ww11

## Exercise 1 - Particle cloud

### Information

The dashboard you are building in node-red needs to be able to test cloud functionality.
This exercise is about publishing messages from the Boron and reading cloud variables from both the CLI and from node-red.

The way data is exposed in the particle cloud is using a REST API, if you are in doubt about REST re-visit the lecture from ITT1 programming about [using web services](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_weekly_plans.pdf#section.16)

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/particle/particle_cloud](https://eal-itt.gitlab.io/iot-with-particle/particle/particle_cloud) and use them to implement cloud test functionality in your node-red-dashboard.  
As a minimum you should be able to display values from your Boron, obtained from the cloud in your node-red-dashboard.


## Exercise 2 - Documentation Catchup

### Information

Looking at your gitlab projects it seems that most of you have some work to do regarding documentation.
This week you will have time to catch up on that.

If you are absolutely sure that your documentation doesn't need attention you might need to catch up on exercises, either ones that you did not complete or those that needs tweaking to live up to specs.  

### Exercise instructions

1. In your group, use `round table` to get an overview of where you are behind in the project.
2. Schedule issues in gitlab, configured as s.m.a.r.t tasks for the work you are behind with.
3. Carry out the needed work to get on par with the plan.