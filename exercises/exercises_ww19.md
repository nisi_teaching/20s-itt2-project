---
Week: 19
tags:
- Proof of concept
- Documentation
---

# Exercises for ww19

## Exercise 1 - Proof of concept video

Proof of concept (POC) ended in week 18 and we would like to see what you accomplished.

You need to produce a video ~3 minutes long, that includes a walkthrough and demo of your POC system. The video should be uploaded to youtube or similar and shared with teachers and the other project groups.
We are not experts in video editing software, however a quick google search revealed [https://www.openshot.org/](https://www.openshot.org/) that works for mac, linux and windows.
Something like [https://obsproject.com/](https://obsproject.com/) might also work for you.

## Instructions

1. Setup your POC system
2. Make a video that explains the moving parts and a small demo of the system
3. Share your video with teachers and other groups on [hackmd](https://hackmd.io/@nisi/Hk1aYH6KU)

The deadline for submission of the video is at 2020-05-05 11:00 AM.
