---
Week: 08
tags:
- node-red
- Electronics
- UART
---

# Exercises for ww08

## Exercise 1 - node-red setup

### Information

This exercise, and exercise 2, is about setting up node-red for measurements and interactions on the Boron.

### Exercise instructions

Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_setup](https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_setup)  

## Exercise 2 - node-red serial communication

### Information

See description in exercise 1

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_boron_serial](https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_boron_serial)


## Exercise 3 - LED and button module

### Information

To be able to interact with the Boron you will need aome buttons and LED's. This exercise is about building a HW module with two buttons and two LED's.

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/electronics/led_btn_module](https://eal-itt.gitlab.io/iot-with-particle/electronics/led_btn_module)

If you need veroboard talk to NISI


