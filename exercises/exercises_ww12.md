---
Week: 12
tags:
- Cloud
- node-red
- Housing
---

# Exercises for ww12

## Exercise 1 - Particle cloud functions

### Information

Particle cloud function are meant for controlling hardware.
This can be as simple as toggling an LED or more complex, like triggering sampling of a sensor.

As you are working remote and only have 1 Boron for each group we recommend you to use [https://build.particle.io](https://build.particle.io) to program the Boron, if it is located at another group member's location.

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/particle/particle_function](https://eal-itt.gitlab.io/iot-with-particle/particle/particle_function) to get familiar with particle cloud functions.

In your Boron appilcation and node-red-dashboard implement:

1. Cloud functions to control the 2 led's on the [led_btn_module](https://eal-itt.gitlab.io/iot-with-particle/electronics/led_btn_module)
2. Cloud function to trigger a sampling of the ADC input.

Both functions should be controlled from the node-red-dashboard with respective button and status from the led's + ADC reading should be displayed

**Remember to document your work**

## Exercise 2 - housing (draft version)

### Information

Boxes with wires are error prone, so we want to make a box that may hold the boron and the support modules.

Made correctly, you will avoid all sorts of wire issues.

And given the current situation, we will not make the box in fablab.

### Exercise instructions

1. Decide what you box needs to contain - boron, sensor modules, leds and so on

2. Make a complete list of what cables and wires that needs to get in and out of the box - power supply, USB, sensors wires, and so on.

3. Design the box, and add holes needed.

    * I use Openscad and specifically [this module](https://github.com/bmsleight/lasercut).
    * [My youtube video](https://www.youtube.com/watch?v=INyVtZfIVvE) on the topic (in Danish)
    * Feel free to do the design for lasercutting or 3D printing, and use whatever software tool that you feel comfortable with.
    * We have mounting pins like [these](https://dk.rs-online.com/web/p/afstandsstykker-skruebar-stikbar-og-blindhul/2652530/) or you can opt for double sided adhesive tape (if you have it).
    * you will need to drill holes in some of you boards, and maybe create a new one for the boron.

  The design must end up being a dxf og svg file.

4. Gitlab support previewing of .stl files, so we want those files added to your gitlab project. See e.g. [BH2019 popboxes](https://gitlab.com/bornhack/popbox/-/blob/master/box_output/box_BH19.stl)

5. This is a theoretical exercise, until we get back to school, so make a draft version. Unless you are experience in this, you will have made mistakes that you must fix the first time you build it - so make a draft, and don't polish it too much.
