---
Week: 07
tags:
- 4-20mA
- Boron
- UART
- Electronics
---

# Exercises for ww07

## Exercise 1 - 4-20mA simulator module

### Information

In week 06 you researched which components to use for the 4-20mA simulator module, this week it is time to simulate, build and test it.

The reason for using this module in the project is to simulate sensors that uses the 4-20mA standard.

**Important** Do not connect this module directly to the Boron

### Exercise instructions

Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/electronics/current_loop_tester](https://eal-itt.gitlab.io/iot-with-particle/electronics/current_loop_tester)  

For components talk to NISI

## Exercise 2 - 4-20mA interface module

### Information

In order to measure the current from a 4-20mA sensor with an ADC, the current needs to be converted to a proportional voltage.

This exercise will assist you in building an interface module that sits between the current loop tester and the Boron's analog to digital converter (ADC)

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/electronics/current_adc_interface](https://eal-itt.gitlab.io/iot-with-particle/electronics/current_adc_interface)


## Exercise 3 - Setup and test Boron sampling sketch

### Information

To be able to test the voltage output from the interface module you need to know how to use the ADC in the Boron.

### Exercise instructions

Follow the instruction at [https://eal-itt.gitlab.io/iot-with-particle/electronics/current_adc_interface](https://eal-itt.gitlab.io/iot-with-particle/electronics/current_adc_interface)

Remember to push your code to gitlab

## Exercise 4 - Boron to/from RPi UART communication

### Information

We want a two-way serial communication.

### Exercise instructions

1. Create a .md document to document your work, especially the troubleshooting you do is important to document.
2. Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/docs/communication/two-way-serial/](https://eal-itt.gitlab.io/iot-with-particle/docs/communication/two-way-serial/)
3. Add your code projects and troubleshooting document to your gitlab project.
