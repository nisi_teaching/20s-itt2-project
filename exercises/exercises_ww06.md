---
Week: 06
tags:
- Boron
- Particle
- Raspberry Pi
- UART
- 4-20mA
- Electronics
---

# Exercises for ww06

## Exercise 1 - Boron setup

### Information

This week you will get the Boron development board which will be used throughout the project.  
Each group will get one Boron to share, at the end of the project you will have to return the Boron to NISI.

This exercise will guide you through the initial setup of the Boron.

### Exercise instructions

Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/particle/boron_setup](https://eal-itt.gitlab.io/iot-with-particle/particle/boron_setup)

## Exercise 2 - Particle workbench setup

### Information

Exercise 1 showed you how to use the web IDE to program the Boron, this is not recommended for development as it will use data, which is limited and costs money.  
Luckily there is the option to program and flash the Boron locally on your computer, using Particle Workbench, which is and IDE addon to Visual Studio Code.

### Exercise instructions

Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/particle/particle_workbench](https://eal-itt.gitlab.io/iot-with-particle/particle/particle_workbench)


## Exercise 3 - Boron to RPi UART communication

### Information

To establish initial serial communication between a raspberry pi and the boron board complete this exercise.

Serial communication is an essential part, of the things you are going to build, on the raspberry pi later in the project. 

### Exercise instructions

1. Create a .md document to document your work, especially the troubleshooting you do is important to document.
2. Follow the instructions at [https://eal-itt.gitlab.io/iot-with-particle/docs/communication/serial/](https://eal-itt.gitlab.io/iot-with-particle/docs/communication/serial/)
3. Add your code projects and troubleshooting document to your gitlab project.

## Exercise 4 - 4-20mA current loop tester components

### Information

Next week you will build a 4-20mA current loop tester but before you can build it you need to ensure that you have the needed components.

We need the completed component list no later than 15:30 2020-02-04 in order to be able to order the components in time.  
If you miss this deadline you might not be able to build the current loop tester next week. 

### Exercise instructions

1. Read the description and guide at [https://eal-itt.gitlab.io/iot-with-particle/electronics/current_loop_tester](https://eal-itt.gitlab.io/iot-with-particle/electronics/current_loop_tester) 
2. Investigate if we have the components in stock in e-lab ?
3. Fill out the component list at [https://hackmd.io/dVZ7O9RlRzWw8PdjUfrULw](https://hackmd.io/dVZ7O9RlRzWw8PdjUfrULw)  
Please notice the supplier list and coordinate between groups to pick only one supplier.   