---
Week: 05
tags:
- Teams
- Gitlab
- Pre morten
- Project plan
---

# Exercises for ww05

## Exercise 1 - Forming teams

### Information

This exercise forms the teams for the project classes in 2nd semester.

Rules for forming teams are:

* max/min 4 students in each group
* Students are not allowed to be in a group with the same persons as in the 1st semester project.

### Exercise instructions

1. Team up 4 students.
2. Write your team members gitlab usernames and school mail [here](https://hackmd.io/@UzoEFGsXSV6pE9hpiz8kSw/HJdX7pSW8) (requires signin)

## Exercise 2 - Create gitlab project

### Information

Create a gitlab project for the semester project.

### Exercise instructions

1. Ask MON or NISI to add your group members to the [20s-itt-dm-project](https://gitlab.com/20s-itt-dm-project) gitlab group
2. Create a gitlab project inside of the 20s-itt-dm-project group - name the project `Group_x` and replace the x with your group number
3. Within your team, agree on a folder/document structure for the gitlab project
4. Document the structure in a markdown document named `Group_x_structure.md` (replace x with group number)


## Exercise 3 - pre mortem

### Information

Imagine that the project has failed and ask, "what did go wrong" ?  
The team members’ task is to generate plausible reasons for the project’s failure.

### Exercise instructions

### part 1

In your team use the cooperative learning structure **think-pair-share** to find out what went wrong ?

1. Think (10 minutes)  
    Write down a numbered list containing every reason you can think of for the failure. Including things you ordinarily wouldn’t mention as potential problems, for fear of being impolitic.
2. Pair (5 minutes)  
    Compare your lists and remove possible duplicates
3. Share (10 minutes)  
    read one reason from your pair list and explain the reason for putting it on the list. Continue until all reasons have been recorded in a team list and order the list by probablilty.

### part 2

On class we will use **30 for tea** to share your teams 3 most probable points of failure

### Links

[pre mortem article](https://hbr.org/2007/09/performing-a-project-premortem)  
[Cooperative learning structures](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 4 - Update project plan

### Information

We have supplied a generic project plan covering part 1 of the project, but you still have to fill in the student parts.
This is considered the first iteration of the project plan, you might need to update it at a later stage in the project.

### Exercise instructions

1. Include the [project plan] in your gitlab project
2. Fill in the parts marked: **TBD: This section must be completed by the students.**

Remember to rename the project plan to include your group name

### Links

