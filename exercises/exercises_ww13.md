---
Week: 13
tags:
- System design
- IIoT
- Flowchart
- Dashboard
- Protocol
- Unit testing
---

# Exercises for ww13

## Exercise 1 - IoT system design

### Information

Since the beginning of this project you have been presented with different possibilities of the system we are using.  
But how exactly are you going to put your system together and how is it going to be used?  
**You have to use all the possibilities to some extent in your system**  

This exercise is a pure planning and design exercise, the goal is to have plans on how to put a system together with the possibilities you have.  
During this exercise you will have to communicate a lot in your group in order to agree on decisions and structure work between group members.  

### Exercise instructions

1. Research  
Research what Industrial Iot is and what use cases exists.  
Search google for "IIoT use cases" to find inspiration for your system.  
Buzzwords are: Predictive maintenance, Asset tracking and monitoring, safety and security, energy efficiency, customer engagement, customer satisfaction, strategic planning, services based model.

2. Hydac presentation  
Take a look at the [Hydac presentation](https://eal-itt.gitlab.io/20s-itt2-project/Pr%C3%A6senation_HYDAC_UCL.pdf) from project start (also use your notes from the presentation), what did they actually ask for and does it give you inspiration for your system?

3. Use case description  
Make a use case description (similar to the ones you goolged). In short, describe how your system are to be used and what it's purpose is.    
Questions you can use to describe your use case:
    * What is the purpose of the system?
    * How can users benefit from it?
    * Is it going to save money/environment etc.
    * Is it going to give added insight in data ?
    * How does it fit with Hydac's use case?  
    **Document your case description in a md. document on gitlab**

4. Block Diagram  
Create a block diagram of the different parts in your system. Remember to note what type of information is sent via the connections.  
As inspiration use the [system overview](https://gitlab.com/EAL-ITT/20s-itt2-project/raw/master/docs/photos/project_overview_no_mesh.png) block diagram.
Block diagrams can be drawn using [https://app.diagrams.net/](https://app.diagrams.net/) or [https://www.yworks.com/products/yed](https://www.yworks.com/products/yed) and similar.  
It is usually easier to start with a handdrawn sketch!  
**Document your block diagram on gitlab**

5. Boron code  
Plan the code you will implement on the Boron. Use flow charts and pseudo code, do not write actual code but try to get the flow and structure described.  
Please put as much of your code in to functions.  
If you need inspiration revisit the lectures about flow charts from ITT1 programming.  
**Document your flow chart and pseudo code on gitlab**

6. UART  
Describe your UART protocol. You might already have a protocol description from earlier [https://eal-itt.gitlab.io/iot-with-particle/docs/communication/two-way-serial/](https://eal-itt.gitlab.io/iot-with-particle/docs/communication/two-way-serial/)
**Document your protocol in a .md document on gitlab**

7. node-red
What are you going to implement in node-red and what elements are you going to put on your dashboard?
Make a sketch on what your dashboard are going to look and what the node-red flow needs to include.
You can use hand drawings for the initial drawings and then one of the programs mentioned in 4. for the final version.
**Document your drawings on gitlab**

8. Testing
How are you going to test your system?
Make a plan for what parts your system will be able to test, with respect to both hardware and software.
**Document your test plan in a .md document on gitlab**

## Exercise 2 - Unit testing

### Information

You will implement simple unit testing of the hardware-independent parts of the code.

The supplied VM will hep ensure that everybody is using the same environment, and hence facilitate debugging by removing certain program installation and version issues.

We expect your code to already be very boron specific. You will most likely be forced to refactor your code, and use multiple `.cpp` and `.h` files.

### Exercise instructions

1. Go [here](https://eal-itt.gitlab.io/iot-with-particle/docs/particle/unittest/) and read the exercise.

2. Download the `.ova` files and add a shared directory to point to where you have your cpp-code.

   If you are still using `.ino`, you must make the shift to `.cpp` now.

3. Do the exercise.
