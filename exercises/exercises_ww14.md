---
Week: 14
tags:
- Documentation
---

# Exercises for ww14

## Exercise 1 - Recreate from documentation

In this exercise you will recreate another groups minimum system using the documentation that the other group has produced.

If you have problems during the exercise you will report these problems through the other groups gitlab by creating issues with a label named `bug`.

## Instructions

1. Create a `bug` label in your gitlab project.
3. Hand over your documentation.

    You need to make it obvious where to find the documentation, e.g. in a top-level `readme` file.

4. Start recreating another groups minimum system from the documentation you have received.

### While recreating

Make a checklist called `recreation-<other groupname>-<your groupname>.md` in the root of your gitlab project.
The checklist should reflect the minimal system that you are recreating !

The content should be a checklist in this format:

```
Recreation checklist for group <other group name>
===================================================

Group name: <your groupname>

Checklist
------------

- [ ] Project plan completed
- [ ] Minimal circuit schematic for all modules completed
- [ ] Boron minimal system code with 4-20 mA module working
- [ ] Boron to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration documented
- [ ] Raspberry pi and node setup setup
- [ ] Node red showing 4-20 mA
- [ ] Boron toolchain setup described and working


Comments
-----------

<add a bulleted list of issues you have created on the projects of other groups>

<Any comments that may be relevant for you, other groups or teachers. (Could be that something is super succesful)>


```

## Exercise 2 - Finish design from last week

see week 13
