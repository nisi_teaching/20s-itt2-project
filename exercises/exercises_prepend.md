---
title: 'ITT2 Project'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'ITT2 project, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References:

* [Weekly plans](https://eal-itt.gitlab.io/20s-itt2-project/20s_ITT2_weekly_plans.pdf)

/pagebreak