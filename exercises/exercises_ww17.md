---
Week: 17
tags:
- Project management
- Documentation
- Milestones
- Tasks
- Gitlab
---

# Exercises for ww17

## Exercise 1 - Milestones and tasks in gitlab

You are all doing different projects from now on, which makes it hard to give you the same tasks.  
Because of this you will have to make and plan your own milestones and tasks.  
The tasks you create monday has to cover this weeks project work, in essence you have to plan work for all group members for both monday and tuesday.  

The first milestone will include making a detailed design for the proof-of-concept system.

We will have extra time at teacher meetings to review the work and milestones you have planned.  
Remember to create them as s.m.a.r.t tasks

## Instructions

1. Expand your group morning meeting to include this exercise
2. Create at least 3 milestones covering the rest of the project period
3. Create the tasks for this week in gitlab
