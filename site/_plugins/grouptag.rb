module Jekyll
  module TagFilters
    def count_by_item(input)
      counts = Hash.new(0)
      input.each { |name| counts[name] += 1 }
      counts
    end
  end
end


Liquid::Template.register_filter(Jekyll::TagFilters)